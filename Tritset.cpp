
#include "Tritset.h"
#include "Trit.h"


Tritset::Tritset()
{
    set = nullptr;
    size_b = 0;
    capasity = 0;
}

Tritset::Tritset(unsigned int s)
{
    size_b = s + Trit_U_Int - (s % Trit_U_Int);
    capasity = s;

    if(size_b > 0)
    {
        set = new unsigned int [size_b / Trit_U_Int];
    }
    else
    {
        set = nullptr;
    }

    for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
    {
        set[i] = 0;
    }
}

Tritset::Tritset(const std::initializer_list<Trit> &list)
{
    size_b = list.size() + Trit_U_Int - (list.size() % Trit_U_Int);
    capasity = list.size();

    if(size_b > 0)
    {
        set = new unsigned int [size_b / Trit_U_Int];
    }
    else
    {
        set = nullptr;
    }

    for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
    {
        set[i] = 0;
    }
    unsigned int count = 0;

    for(auto & element : list)
    {
        push(count, element);
        ++count;
    }
}

Tritset::Tritset(const Tritset & t)
{
    size_b = t.size_b;
    capasity = t.capasity;

    auto new_set = new unsigned int [size_b / Trit_U_Int];

    for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
    {
        new_set[i] = t.set[i];
    }
    set = new_set;
}

Tritset::~Tritset()
{
    if(set != nullptr)
    {
        delete [] set;
        size_b = 0;
        capasity = 0;
    }
}

unsigned int Tritset::get_size_b() const
{
    return size_b;
}

unsigned int Tritset::length() const
{
    if(size_b != 0)
    {
        for(unsigned int i = capasity - 1; i != 0; i--)
        {
            if(get(i) != Trit::U)
            {
                return i + 1;
            }
        }
        return 0;
    }
    return 0;
}

std::unordered_map<Trit, unsigned int> Tritset::cardinality() const
{
    std::unordered_map<Trit, unsigned int> ump;
            ump = {{Trit::U,cardinality(Trit::U)},
                                         {Trit::T, cardinality(Trit::T)},
                                         {Trit::F, cardinality(Trit::F)}};
    return ump;
}

size_t Tritset::cardinality(Trit t) const
{
    size_t count = 0;
    for(unsigned int i = 0; i < capasity; i++)
    {
        if(get(i) == t)
        {
            count++;
        }
    }

    return count;
}

void Tritset::trim( size_t lastIndex)
{
    unsigned int new_size;
    if(size_b <= lastIndex)
    {
        return;
    }
    else
    {
        if(lastIndex%Trit_U_Int == 0)
        {
            for(unsigned int i = lastIndex/Trit_U_Int + 1; i < size_b; i++)
            {
                set[i] = 0;
            }
            //this->size_b = lastIndex;
        }
        else
        {
            for(unsigned int i = lastIndex/Trit_U_Int + 1; i < size_b; i++)
            {
                set[i] = 0;
            }
            set[lastIndex/Trit_U_Int] = (set[lastIndex/Trit_U_Int] >> ((Trit_U_Int - lastIndex%Trit_U_Int - 1)*2));
            set[lastIndex/Trit_U_Int] = set[lastIndex/Trit_U_Int] << ((Trit_U_Int - lastIndex%Trit_U_Int - 1)*2);
            //this->size_b = (lastIndex/Trit_U_Int + 1) * Trit_U_Int;
        }
    }
}

Trit Tritset::get(unsigned int position) const
{
    if(set == nullptr)
    {
        return Trit::U;
    }
    if(size_b < position)
    {
        return Trit::U;
    }

    unsigned int sdvig = (position % Trit_U_Int)*2;
    unsigned int tmp = (set[position/Trit_U_Int]  >>  sdvig) & 0x03;
    Trit res = inttoTrit(tmp);
    return res;
}

void Tritset::push(unsigned int position, Trit t)
{
    unsigned int tmp = TrittoInt(t);

    if(t == Trit::T || t == Trit::F)
    {
        if(set == nullptr)
        {
            size_b = position + Trit_U_Int - (position % Trit_U_Int);
            set = new unsigned int [size_b / Trit_U_Int];

            for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
            {
                set[i] = 0;
            }
        }
        else if(position > size_b)
        {
            unsigned int new_size_b = position + Trit_U_Int - (position % Trit_U_Int);

            unsigned int * new_set = new unsigned int [new_size_b / Trit_U_Int];

            for(unsigned int i = 0; i < new_size_b / Trit_U_Int; ++i)
            {
                new_set[i] = 0;
            }

            for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
            {
                new_set[i] = set[i];
            }

            delete [] set;

            set = new_set;
            size_b = new_size_b;
        }
    }

    unsigned int sdvig = (position % Trit_U_Int)*2;
    unsigned int mask = tmp << sdvig;
    unsigned int res = set[position/Trit_U_Int];
    res &= ~(0x03 << sdvig);
    res |= mask;
    set[position/Trit_U_Int] = res;
}

unsigned int Tritset::cmp_size_b(Tritset const &obj) const
{
    if(size_b > obj.size_b)
    {
        return size_b;
    }
    else
    {
        return obj.size_b;
    }
}

unsigned int Tritset::cmp_capasity(Tritset const &obj) const
{
    if(capasity > obj.capasity)
    {
        return capasity;
    }
    else
    {
        return obj.capasity;
    }
}
Tritset Tritset::operator&(const Tritset &obj)const
{
    unsigned int size = obj.cmp_capasity(*this);
    Tritset tmp (size);
    for(unsigned int i = 0; i < size; i++)
    {
        tmp.push(i, this->get(i) & obj.get(i));
    }
    return tmp;
}

Tritset Tritset::operator|(const Tritset &obj) const
{
    unsigned int size = obj.cmp_capasity(*this);
    Tritset tmp(size);
    for(unsigned int i = 0; i < size; i++)
    {
        tmp.push(i, get(i) | obj.get(i));
    }
    return tmp;
}

Tritset Tritset::operator&=(const Tritset  &obj)
{
    *this = *this & obj;
    return *this;
}

Tritset Tritset::operator|=(const Tritset  &obj)
{
    *this = *this | obj;
    return *this;
}

Tritset Tritset::operator!() const
{
    Tritset tmp(capasity);
    for(unsigned int i = 0; i < size_b; i++)
    {
        tmp.push(i,!(this->get(i)));
    }
    return tmp;
}

bool Tritset::operator==(const Tritset&obj) const
{
    if(this->length() != obj.length())
    {
        return false;
    }
    else
    {
        for(unsigned int i = 0; i < (this->size_b)/Trit_U_Int;i++)
        {
            if(this->set[i] != obj.set[i])
            {
                return false;
            }
        }
    }
    return true;
}

bool Tritset::operator!=(const Tritset&obj) const
{
    if(this->length() != obj.length())
    {
        return true;
    }
    else
    {
        for(unsigned int i = 0; i < this->length();i++)
        {
            if(this->set[i] != obj.set[i])
            {
                return true;
            }
        }
    }
    return false;
}

Tritset& Tritset::operator=(Tritset const & t) {
    if (this == &t)
    {
        return *this;
    }

    size_b = t.size_b;

    auto * new_set = new unsigned int [size_b / Trit_U_Int];

    for(unsigned int i = 0; i < size_b / Trit_U_Int; ++i)
    {
        new_set[i] = t.set[i];
    }

    set = new_set;

    return *this;
}

Trit Tritset::operator[](unsigned int i) const
{
    return get(i);
}

Iterator Tritset::begin()
{
    Iterator it(this);
    it.first();
    return it;
}

Iterator Tritset::end()
{
    Iterator it(this);
    it.last();
    return it;
}

TritsetProxy Tritset::operator[](unsigned int i)
{
    TritsetProxy m(*this, i);
    Trit t = get(i);
    m = t;
    return m;
}

std::ostream &operator <<(std:: ostream &out, Trit &t)
{
    switch(t)
    {
        case(Trit::T):
            out << 2;
            break;
        case(Trit::F):
            out << 1;
            break;
        case(Trit::U):
            out << 0;
            break;
        default:
            out << 0;
    }
    return out;
}

std::ostream &operator <<(std:: ostream &out, Tritset &t)
{
    for(unsigned int i = 0; i < t.get_size_b(); i++)
    {
        unsigned int now = TrittoInt(t.get(i));
        out << now;
    }

    return out;
}
