
#ifndef CPP_TASK2_TRITSET_TRITSET_H
#define CPP_TASK2_TRITSET_TRITSET_H

#include "Trit.h"

#include <iostream>
#include <cstdlib>
#include <iterator>
#include <initializer_list>
#include <unordered_map>


const unsigned int Trit_U_Int = sizeof(unsigned int)*4;


class Iterator;
class TritsetProxy;

class Tritset
{
    friend class Iterator;
    friend class TritsetProxy;

private:
    unsigned int * set;
    unsigned int size_b;
    unsigned int capasity;
public:
    Tritset();
    explicit Tritset(unsigned int s);
    Tritset(const std::initializer_list<Trit> &list);
    Tritset(const Tritset & t);
    ~Tritset();

    unsigned int get_size_b() const;
    unsigned int length () const;
    Trit get(unsigned int position) const;
    void push(unsigned int position, Trit t);
    unsigned int cmp_size_b(Tritset const &obj) const;
    unsigned int cmp_capasity(Tritset const &obj) const;

    size_t cardinality(Trit t) const;

    std::unordered_map<Trit, unsigned int> cardinality() const;

    void trim(size_t lastIndex);

    Tritset operator&(const Tritset  &obj) const;
    Tritset operator&=(const Tritset &obj);

    Tritset operator|(const Tritset &obj)const;
    Tritset operator|=(const Tritset  &obj);

    Tritset operator!() const;

    bool operator==(const Tritset &obj) const;
    bool operator!=(const Tritset &obj) const;

    Tritset& operator=(Tritset const & t);
    Trit operator[](unsigned int i) const;

    Iterator begin();
    Iterator end();

    TritsetProxy operator[](unsigned int i);
};


class Iterator
{
private:
    Tritset * tritset;
    unsigned int cur;

public:
    Iterator(Tritset * t = nullptr)
        : tritset(t)
        , cur(0)
    {
    }

    Trit operator+(unsigned int n)
    {
        if(tritset != nullptr)
            return tritset->get(cur + n);
        else
            return Trit::U;
    }

    Trit operator-(unsigned int n)
    {
        if(tritset != nullptr)
            return tritset->get(cur - n);
        else
            return Trit::U;
    }

    void operator++()
    {
        if(tritset != nullptr)
            ++cur;
    }

    void operator--()
    {
        if(tritset != nullptr)
            --cur;
    }

    bool operator!=(Iterator it)
    {
        return tritset != it.tritset || cur != it.cur;
    }

    bool operator==(Iterator it)
    {
        return tritset == it.tritset && cur == it.cur;
    }

    Trit operator*()
    {
        if(tritset != nullptr)
            return tritset->get(cur);
        else
            return Trit::U;
    }

    void first()
    {
        cur = 0;
    }

    void last()
    {
        if(tritset != nullptr)
            cur = (unsigned) tritset->capasity;
        else
            cur = 0;
    }
};


class TritsetProxy
{
private:
    Tritset &tritset;
    unsigned int index;

public:
    explicit TritsetProxy(Tritset & t, unsigned int i)
        : tritset(t)
        , index(i)
    {
    }

    ~TritsetProxy()
    {
    }

    TritsetProxy & operator=(Trit const & t)
    {
        if(tritset.set == nullptr)
        {
            tritset = Tritset(index);
        }
        unsigned int tmp = TrittoInt(t);
        unsigned int sdvig = (index % Trit_U_Int)*2;
        unsigned int mask = tmp << sdvig;
        unsigned int res = tritset.set[index / Trit_U_Int];
        res &= ~(0x03 << sdvig);
        res |= mask;
        tritset.set[index / Trit_U_Int] = res;

        return *this;
    }

    bool operator==(const Trit & t) const
    {
        Trit k = tritset.get(index);
        if(k == t)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};


std::ostream &operator <<(std:: ostream &out, Trit &t);
std::ostream &operator <<(std:: ostream &out, Tritset &t);


#endif //CPP_TASK2_TRITSET_TRITSET_H
