
#ifndef CPP_TASK2_TRITSET_TRIT_H
#define CPP_TASK2_TRITSET_TRIT_H

#include <iostream>

enum class Trit
{
    F, U, T
};

unsigned int TrittoInt(Trit a);
Trit inttoTrit(unsigned int x);

Trit operator &(Trit a, Trit b);
Trit operator |(Trit a, Trit b);
Trit operator !(Trit a);


#endif //CPP_TASK2_TRITSET_TRIT_H
