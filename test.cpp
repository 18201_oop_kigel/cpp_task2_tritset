#define CATCH_CONFIG_MAIN

#define TRUE Trit::T
#define FALSE Trit::F
#define UNKNOWN Trit::U
#include "catch.hpp"

#include "Tritset.h"
#include <sstream>
#include <list>


TEST_CASE("Trit")
{
    Trit first = Trit:: U;
    Trit second = Trit:: F;
    Trit third = Trit:: T;

    REQUIRE(!first == Trit::U);
    REQUIRE(!second == Trit::T);
    REQUIRE(!third == Trit::F);

    Trit check = first|second;

    REQUIRE(check == Trit::U);
    REQUIRE((first|third) == Trit::T);
    REQUIRE((first&second) == Trit::F);
    REQUIRE((first&third) == Trit::U);

    REQUIRE(TrittoInt(first) == 0);
    REQUIRE(TrittoInt(third) == 2);

    REQUIRE(inttoTrit(1) == Trit::F);
    REQUIRE(inttoTrit(2) == Trit::T);
    REQUIRE(inttoTrit(0) == Trit::U);
}
TEST_CASE("constructor")
{
    Tritset t(10);
    int length = t.get_size_b();
    REQUIRE(length >= 10);
}


TEST_CASE ("Cardinality")
{
    Tritset t{Trit::F,Trit::F,Trit::U,Trit::U,Trit::T,Trit::T,Trit::T,Trit::T,Trit::T};
    REQUIRE(t.cardinality(Trit::T) == 5);
    REQUIRE(t.cardinality(Trit::U) == 2);
    REQUIRE(t.cardinality(Trit::F) == 2);
    std::unordered_map<Trit, unsigned int> ump = {{Trit:: U, 2}, {Trit::T, 5}, {Trit::F, 2}};
    REQUIRE(t.cardinality() == ump);
}

TEST_CASE("push & get")
{
    Tritset t{Trit::F,Trit::F,Trit::U,Trit::U,Trit::T,Trit::T,Trit::F,Trit::T,Trit::T};
    t.push(6, Trit::T);
    REQUIRE(t[6] == Trit::T);
    REQUIRE(t.get(3) == Trit::U);
}

TEST_CASE ("initilizer_list")
{
    Tritset t{Trit::F,Trit::F,Trit::U,Trit::U,Trit::T};
    REQUIRE(t[0] == Trit::F);
    REQUIRE(t[1] == Trit::F);

    REQUIRE(t.get_size_b() == 16);

    REQUIRE(t[2] == Trit::U);
    REQUIRE(t[4] == Trit::T);
}

TEST_CASE ("length ")
{
    Tritset t{Trit::T,Trit::F,Trit::U,Trit::U,Trit::U};
    REQUIRE(t.length() == 2);
}
TEST_CASE ("Proxy")
{
    Tritset t{Trit::T,Trit::F,Trit::U,Trit::U,Trit::U};
    t[0] = Trit::F;
    REQUIRE(t[0] == Trit::F);

}

TEST_CASE("==")
{
    Tritset t1{Trit::U, Trit::F,Trit::T};
    Tritset t2{Trit::U, Trit::F,Trit::T};
    bool is_r = (t1 == t2);

    REQUIRE(is_r == true);
}


TEST_CASE("basics") {
    const Tritset a{UNKNOWN, UNKNOWN, UNKNOWN, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE};
    const Tritset b{UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE, UNKNOWN, TRUE, FALSE};

    const Tritset aandb{UNKNOWN, UNKNOWN, FALSE, UNKNOWN, TRUE, FALSE, FALSE, FALSE, FALSE};
    const Tritset aorb{UNKNOWN, TRUE, UNKNOWN, TRUE, TRUE, TRUE, UNKNOWN, TRUE, FALSE};
    const Tritset nota{UNKNOWN, UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE};

    CHECK((a & b) == aandb);
    CHECK((a | b) == aorb);
    CHECK((!a) == nota);

    Tritset a1 = a;
    a1 &= b;
    CHECK(a1 == aandb);

    Tritset a2 = a;
    a2 |= b;
    CHECK(a2 == aorb);
}

TEST_CASE("resize")
{
    Tritset a;
    a[9] = TRUE;
    REQUIRE(a.length() == 10);
}
TEST_CASE("for-each")
{
    Tritset a{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    std::list<Trit> exp{UNKNOWN, UNKNOWN, FALSE, FALSE, FALSE, TRUE};
    int l = exp.size();
    REQUIRE(a.length() == l);

    int i = 0;
    for (auto t: a)
    {
        REQUIRE (i < l);
        INFO("Check position " << i);
        CHECK(t == exp.front());
        exp.pop_front();
        i++;
    }
}

TEST_CASE ("Logica")
{
    Tritset t(10);

    for (unsigned int i = 0; i < 10; i++)
    {
        t.push(i,Trit::T);
    }

    Tritset t1(20);

    for (unsigned int i = 0; i < 20; i++)
    {
        t1.push(i,Trit::F);
    }

    Tritset setC = t & t1;
    Tritset setD = t| t1;
    Tritset setE = !t;

    REQUIRE(setC.get_size_b() == t1.get_size_b());
    REQUIRE(setD.get_size_b() == t1.get_size_b());
    REQUIRE(setE.get_size_b() == t.get_size_b());
    REQUIRE(setC.cardinality(Trit::F) == 20);
    REQUIRE(setD.cardinality(Trit::T) == 10);
    REQUIRE(setE.cardinality(Trit::F) == 10);
};


